CREATE DATABASE IF NOT EXISTS notas;
USE notas

CREATE TABLE usuario(
    id INT(25) AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(100),
    apellidos VARCHAR(200),
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    fecha DATE NOT NULL,
    CONSTRAINT pk_usuario PRIMARY KEY (id),
    CONSTRAINT uq_email UNIQUE(email) 
)ENGINE = InnoDb;

CREATE TABLE nota(
    id INT(25) AUTO_INCREMENT NOT NULL,
    usuario_id INT(25) NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    descripcion MEDIUMTEXT,
    fecha DATE NOT NULL,
    CONSTRAINT pk_nota PRIMARY KEY(id),
    CONSTRAINT fk_nota_usuario FOREIGN KEY(usuario_id) REFERENCES usuario(id)
)ENGINE = InnoDb;