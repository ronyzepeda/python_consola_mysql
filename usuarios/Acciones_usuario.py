import usuarios.Usuario as model
import notas.Acciones as accion_nota

class Acciones_usuario:

    def registro(self):
        print("\nsolicitaremos algunos datos para hacer tu registro en nuestro sistema:  ")
        nombre = input("ingresa tu nombre: ")
        apellidos = input("ingresa tu apellidos: ")
        email = input("ingresa tu email: ")
        password = input("ingresa tu contraseña: ")

        usuario = model.Usuario(nombre,apellidos,email,password)
        registro = usuario.registro()

        if registro[0]>=1:
            print(f"te hemos registrado {registro[1].nombre} el correo asignado para inicio de sesion es {registro[1].email} ")
        else:
            print(f"no te hemos podido registrar")
    
    def login(self):
        try:
            print("\nvamos a validar tus credenciales")
            email = input("ingresa tu email: ")
            password = input("ingresa tu contraseña: ")

            usuario = model.Usuario("","",email,password)
            login = usuario.login()

            if login[3] == email :
                print(f"\n bienvenido al sistema {login[1]} has accedio el {login[5]}")
                self.proximas_Acciones(login)

        except TypeError:
            print("\n llena los campos y verifica tu usuario y contraseña")
            

    def proximas_Acciones(self,data):
        print("""
        ACCIONES DISPONIBLES 
            1- CREAR NOTA
            2- MOSTRAR TUS NOTAS
            3- ELIMINAR NOTAS
            4- SALIR
        """)

        try:
            sleccion_acciones = int(input("ingresa el numero de la opcion que deseas ejecutar:  "))

            realizar = accion_nota.Acciones()

            if sleccion_acciones == 1:
                realizar.crear(data)
                self.proximas_Acciones(data)
                            
            elif sleccion_acciones == 2:
                realizar.mostrar(data)
                self.proximas_Acciones(data)

            elif sleccion_acciones == 3:
                realizar.borrar(data)
                self.proximas_Acciones(data)
            
            elif sleccion_acciones == 4:
                print(f"hasta luego {data[1]} ")
                exit()
         
            else:
                print("ingresa una opcion valida, puedes verifica en el menu las acciones disponibles TONTO")

        except ValueError:
            print("ingresa un numero por favor")     

