import mysql.connector

def conexion():
    database = mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "",
    database = "notas"
    )
    cursor = database.cursor(buffered = True)

    return [database,cursor]