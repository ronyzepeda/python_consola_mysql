import datetime
import hashlib
import usuarios.Conexion as con

dbconexion = con.conexion()
database = dbconexion[0]
cursor = dbconexion[1]

class Usuario:

    def __init__(self,nombre,apellido,email,password):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.password = password

    def registro(self):
        fecha = datetime.datetime.now()
        sql = "INSERT INTO usuario VALUES(null,%s,%s,%s,%s,%s)"
        cifrado = hashlib.sha256()
        cifrado.update(self.password.encode("utf-8"))
        usuario = (self.nombre, self.apellido,self.email,cifrado.hexdigest(),fecha)
        
        try:
            cursor.execute(sql,usuario)
            database.commit()
            result = [cursor.rowcount, self]
        except:
            result = [0, self]

        return result
    
    def login(self):
        sql = "SELECT * FROM usuario WHERE email = %s AND password = %s"

        cifrar = hashlib.sha256()
        cifrar.update(self.password.encode("utf-8"))
        usuario = (self.email, cifrar.hexdigest())
        cursor.execute(sql,usuario)
        result = cursor.fetchone()
        
        return result


    







