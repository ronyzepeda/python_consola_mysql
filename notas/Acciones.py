import notas.Nota as modelo

class Acciones:

    def crear(self, usuario):
        print(f"\nvamos a crear una nota {usuario[1]}")
        print("---------------------NUEVA NOTA--------------------------------")
        titulo = input("\ntitulo de la nueva nota: ")
        descripcion = input("\ncontenido de la nueva nota: ")
        print("----------------------------------------------------------------")
        
        nota = modelo.Nota(usuario[0],titulo,descripcion)
        guardar = nota.guardar()

        if guardar[0] >= 1:
            print(f"\nalmacenamos con exito la nota: {nota.titulo}")
        else:
            print(f"\nlo sentmos mucho {usuario[1]} no hemos podido almacenar tu nota")
        
    def mostrar(self, usuario):
        print(f"\nestas son las notas que tienes almacenadas {usuario[1]}:")
        nota = modelo.Nota(usuario[0])
        notas = nota.listar()

        print("\n**************INICIO*******************")
        for nota in notas:
            print("-----------------------------------")
            print(f"titulo: {nota[2]}")
            print(f"contenido: {nota[3]}")
            print("------------------------------------")
        print("***************FIN***********************")

    def borrar(self, usuario):
        print(f"\nvamos a eliminar una de tus notas {usuario[1]} ")
        titulo = input("\ningresa el titulo de la nota que deseas elimiar: ")

        nota = modelo.Nota(usuario[0],titulo)
        result = nota.eliminar()

        if result[0]>= 1:
            print(f"\nhemos eliminado con exito la nota: '' {titulo} '' de tus notas {usuario[1]}")
        else:
            print(f"\nlo sentimos {usuario[1]} no es posible eliminar la nota")

        
